import "bootstrap/dist/css/bootstrap.min.css"
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import FontAwesomeIcon from '@/plugins/fontawesome-icons'

createApp(App).component('FontAwesomeIcon', FontAwesomeIcon).use(router).mount("#app");

import "bootstrap/dist/js/bootstrap.js"