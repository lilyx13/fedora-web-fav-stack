# Components
_This file outlines how to make the best use of components as well as tips on editing them_

## July 8 Component Building Notes
 Today I (lilyx) took a macro approach to our components. Starting by drafting out the content needed on the **workstation** page
	- My focus at this stage is to determine the most productive and consistent workflow for component usage
		- **workflow 1**: Simplest components:
			- dynamic content passed as a prop
			- static text is slotted
			- **would feel like using an `<a></a>` tag**
		- **workflow 2**: Looped components such as lists:
			- pass an array of information to the component
			- component generates multiple child elements based on the arrays and objects passed into it
			- slots used for content that doesn't quite fit the majority use cases
			- **takes advantage of `v-for` syntax and ability to pass lists of info, reduced html editing later**
		- **workflow 3**: Complex components (cards, heros)
			- Named slots are used for sections (I think if there's 3+ rows in a component, this is a good time for this configuration
			- Background content is passed as a prop
			- Component is utilitiy focused, focusing on how the elements are aligned at different screen sizes
			- Named slots allow for a diverse amount of layout changes
				- ie: in the hero sections, we currently have 3 ways that buttons are positioned, slotted styles can give us default options with each named slot to account for variation while still maintaining standardized ways of doing things